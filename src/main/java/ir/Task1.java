package ir;

import ir.indexer.Indexer;
import ir.indexer.weighting.WeightingTfIdf;
import java.io.File;
import org.apache.log4j.Logger;

public class Task1 {

    private static final Logger log = Logger.getLogger(Task1.class);

    public static void main(String[] args) {
        log.info("Program started.");
        
        log.info("Processing arguments.");
        Arguments arguments = processArguments(args);
        int termFrequencyLowerBound = arguments.termFrequencyLowerBound;
        int termFrequencyUpperBound = arguments.termFrequencyUpperBound;
        String pathToRootDir = arguments.rootDirectory;

        log.info("Initializing Indexer.");
        Indexer indexer = new Indexer(termFrequencyLowerBound, termFrequencyUpperBound);

        log.info("Processing files.");
        indexer.parseDirectories(new File(pathToRootDir));
        
        log.info("Computing weights.");
        indexer.computeWeights(new WeightingTfIdf());
        
        log.info("Generating .arff output file.");
        indexer.writeArff("index.arff");

        log.info("Program exited.");
    }

    private static Arguments processArguments(String[] args) {
        int termFrequencyLowerBound = -1;
        int termFrequencyUpperBound = -1;
        String rootDirectory = "";
        
        if (args.length != 3) {
            log.debug("Invalid number of arguments. Expected: 3");
            printUsageAndExit();
        }

        try {
            termFrequencyLowerBound = Integer.parseInt(args[0]);
            termFrequencyUpperBound = Integer.parseInt(args[1]);
            rootDirectory = args[2];
        } catch (Exception e) {
            log.debug("Could not process input arguments. Shutdown program.", e);
            printUsageAndExit();
        }

        if (termFrequencyLowerBound < 0 || termFrequencyUpperBound < 0) {
            log.debug("Invalid argument for lower- or upper-bound.");
            printUsageAndExit();
        }

        return new Arguments(termFrequencyLowerBound, termFrequencyUpperBound, rootDirectory);
    }

    private static void printUsageAndExit() {
        log.info("USAGE: ir.Task1 <lowerBound> <upperBound> <pathToDocumentsRoot>");
        System.exit(-1);
    }

    private static class Arguments {

        private int termFrequencyLowerBound;
        private int termFrequencyUpperBound;
        private String rootDirectory;

        public Arguments(int termFrequencyLowerBound, int termFrequencyUpperBound, String rootDirectory) {
            this.termFrequencyLowerBound = termFrequencyLowerBound;
            this.termFrequencyUpperBound = termFrequencyUpperBound;
            this.rootDirectory = rootDirectory;
        }

        public String getRootDirectory() {
            return rootDirectory;
        }

        public int getTermFrequencyLowerBound() {
            return termFrequencyLowerBound;
        }

        public int getTermFrequencyUpperBound() {
            return termFrequencyUpperBound;
        }
    }
}
