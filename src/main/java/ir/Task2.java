package ir;

import ir.indexer.Indexer;
import ir.indexer.entity.ScoredDocument;
import ir.indexer.weighting.WeightingTfIdf;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael Sobotka
 */
public class Task2 {

    private static final Logger log = Logger.getLogger(Task2.class);

    public static void main(String[] args) {
        log.info("Program started.");
        
        log.info("Processing arguments.");
        Arguments arguments = processArguments(args);
        int termFrequencyLowerBound = arguments.termFrequencyLowerBound;
        int termFrequencyUpperBound = arguments.termFrequencyUpperBound;
        String postingListSize = arguments.postingListSize;
        String pathToRootDir = arguments.rootDirectory;

        log.info("Initializing Indexer.");
        Indexer indexer = new Indexer(termFrequencyLowerBound, termFrequencyUpperBound);

        log.info("Processing files.");
        indexer.parseDirectories(new File(pathToRootDir));
        
        log.info("Average document length is: " + indexer.getAverageDocumentLength());

        log.info("Computing weights.");
        indexer.computeWeights(new WeightingTfIdf());

        log.info("Precomputing euclidean lengths.");
        indexer.preComputeEuclideanLengths();

        // Topics used as input queries
        List<String> topics = new ArrayList<String>();
        topics.add("misc.forsale/76057");
        topics.add("talk.religion.misc/83561");
        topics.add("talk.politics.mideast/75422");
        topics.add("sci.electronics/53720");
        topics.add("sci.crypt/15725");
        topics.add("misc.forsale/76165");
        topics.add("talk.politics.mideast/76261");
        topics.add("alt.atheism/53358");
        topics.add("sci.electronics/54340");
        topics.add("rec.motorcycles/104389");
        topics.add("talk.politics.guns/54328");
        topics.add("misc.forsale/76468");
        topics.add("sci.crypt/15469");
        topics.add("rec.sport.hockey/54171");
        topics.add("talk.religion.misc/84177");
        topics.add("rec.motorcycles/104727");
        topics.add("comp.sys.mac.hardware/52165");
        topics.add("sci.crypt/15379");
        topics.add("sci.space/60779");
        topics.add("sci.med/59456");

        // Process all queries
        for (int currentTopic = 0; currentTopic < topics.size(); currentTopic++) {

            String filePath = pathToRootDir + File.separator + topics.get(currentTopic);
            File currentFile = new File(filePath);
            List<ScoredDocument> rankedDocuments = indexer.processQueryCosine(currentFile, 10);

            String fileName = "";
            try {
                fileName = generateOutputFileName(currentTopic + 1, postingListSize);
                BufferedWriter out = new BufferedWriter(new FileWriter(fileName));

                for (ScoredDocument doc : rankedDocuments) {
                    out.append(generateLineOfOutput(currentTopic + 1, postingListSize, doc));
                }

                out.close();
            } catch (IOException e) {
                log.error("Could not write output file: " + fileName);
            }
        }
    }

    // Filename: <postingListSize>_topic<XX>_group<YY>.txt
    private static String generateOutputFileName(int topicNumber, String postingListSize) {
        StringBuilder sb = new StringBuilder();

        sb.append(postingListSize);
        sb.append("_topic");
        sb.append(Integer.toString(topicNumber));
        sb.append("_groupS.txt");

        return sb.toString();
    }

    // topic<XX> Q0 <doc_id> <rank> <similarity> group<YY>_<postingListSize>
    private static String generateLineOfOutput(int topicNumber, String postingListSize, ScoredDocument doc) {
        StringBuilder out = new StringBuilder();
        out.append("topic");
        out.append(Integer.toString(topicNumber));
        out.append(" Q0 ");
        out.append(doc.getDocument().getDocClass());
        out.append("/");
        out.append(doc.getDocument().getId());
        out.append(" ");
        out.append(Integer.toString(doc.getRank()));
        out.append(" ");
        out.append(Double.toString(doc.getScore()));
        out.append(" groupS_");
        out.append(postingListSize);
        out.append("\n");
        return out.toString();
    }

    private static Arguments processArguments(String[] args) {
        int termFrequencyLowerBound = -1;
        int termFrequencyUpperBound = -1;
        String postingListSize = "";
        String rootDirectory = "";

        if (args.length != 4) {
            log.debug("Invalid number of arguments. Expected: 4");
            printUsageAndExit();
        }

        try {
            termFrequencyLowerBound = Integer.parseInt(args[0]);
            termFrequencyUpperBound = Integer.parseInt(args[1]);
            postingListSize = args[2];
            rootDirectory = args[3];
        } catch (Exception e) {
            log.debug("Could not process input arguments. Shutdown program.", e);
            printUsageAndExit();
        }

        if (termFrequencyLowerBound < 0 || termFrequencyUpperBound < 0) {
            log.debug("Invalid argument for lower- or upper-bound.");
            printUsageAndExit();
        }

        return new Arguments(termFrequencyLowerBound, termFrequencyUpperBound, postingListSize, rootDirectory);
    }

    private static void printUsageAndExit() {
        log.info("USAGE: ir.Task2 <lowerBound> <upperBound> <postingListSize> <pathToDocumentsRoot>");
        System.exit(-1);
    }

    private static class Arguments {

        private int termFrequencyLowerBound;
        private int termFrequencyUpperBound;
        private String postingListSize;
        private String rootDirectory;

        public Arguments(int termFrequencyLowerBound, int termFrequencyUpperBound, String postingListSize, String rootDirectory) {
            this.termFrequencyLowerBound = termFrequencyLowerBound;
            this.termFrequencyUpperBound = termFrequencyUpperBound;
            this.postingListSize = postingListSize;
            this.rootDirectory = rootDirectory;
        }

        public String getPostingListSize() {
            return postingListSize;
        }

        public String getRootDirectory() {
            return rootDirectory;
        }

        public int getTermFrequencyLowerBound() {
            return termFrequencyLowerBound;
        }

        public int getTermFrequencyUpperBound() {
            return termFrequencyUpperBound;
        }
    }
}