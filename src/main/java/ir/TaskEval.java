package ir;

import ir.eval.TrecEvaluator;
import java.io.File;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael Sobotka
 */
public class TaskEval {

    private static final Logger log = Logger.getLogger(TaskEval.class);

    public static void main(String[] args) {
        log.info("Program started.");
        Arguments arguments = processArguments(args);
        String rootDirectory = arguments.getRootDirectory();

        log.info("Aggregating evaluation results.");
        TrecEvaluator evaluator = new TrecEvaluator(new File(rootDirectory));
        evaluator.generateEvaluationResults();

    }

    private static Arguments processArguments(String[] args) {
        String rootDirectory = "";

        if (args.length != 1) {
            log.debug("Invalid number of arguments. Expected: 1");
            printUsageAndExit();
        }

        rootDirectory = args[0];

        return new Arguments(rootDirectory);
    }

    private static void printUsageAndExit() {
        log.info("USAGE: ir.Task3 <lowerBound> <upperBound> <postingListSize> <BM25_b> <BM25_k1> <pathToDocumentsRoot>");
        System.exit(-1);
    }

    private static class Arguments {

        private String rootDirectory;

        public Arguments(String rootDirectory) {
            this.rootDirectory = rootDirectory;
        }

        public String getRootDirectory() {
            return rootDirectory;
        }
    }
}
