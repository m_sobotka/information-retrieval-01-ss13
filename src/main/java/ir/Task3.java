package ir;

import ir.indexer.Indexer;
import ir.indexer.entity.ScoredDocument;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael Sobotka
 */
public class Task3 {

    private static final Logger log = Logger.getLogger(Task3.class);

    public static void main(String[] args) {
        log.info("Program started.");

        log.info("Processing arguments.");
        Arguments arguments = processArguments(args);
        int termFrequencyLowerBound = arguments.getTermFrequencyLowerBound();
        int termFrequencyUpperBound = arguments.getTermFrequencyUpperBound();
        double bm25_b = arguments.getBm25_b();
        double bm25_k1 = arguments.getBm25_k1();
        String postingListSize = arguments.getPostingListSize();
        String pathToRootDir = arguments.getRootDirectory();

        log.info("Initializing Indexer.");
        Indexer indexer = new Indexer(termFrequencyLowerBound, termFrequencyUpperBound);

        log.info("Processing files.");
        indexer.parseDirectories(new File(pathToRootDir));

        log.info("Average document length is: " + indexer.getAverageDocumentLength());

        // Topics used as input queries
        List<String> topics = new ArrayList<String>();
        topics.add("misc.forsale/76057");
        topics.add("talk.religion.misc/83561");
        topics.add("talk.politics.mideast/75422");
        topics.add("sci.electronics/53720");
        topics.add("sci.crypt/15725");
        topics.add("misc.forsale/76165");
        topics.add("talk.politics.mideast/76261");
        topics.add("alt.atheism/53358");
        topics.add("sci.electronics/54340");
        topics.add("rec.motorcycles/104389");
        topics.add("talk.politics.guns/54328");
        topics.add("misc.forsale/76468");
        topics.add("sci.crypt/15469");
        topics.add("rec.sport.hockey/54171");
        topics.add("talk.religion.misc/84177");
        topics.add("rec.motorcycles/104727");
        topics.add("comp.sys.mac.hardware/52165");
        topics.add("sci.crypt/15379");
        topics.add("sci.space/60779");
        topics.add("sci.med/59456");

        // Process all queries
        for (int currentTopic = 0; currentTopic < topics.size(); currentTopic++) {

            String filePath = pathToRootDir + File.separator + topics.get(currentTopic);
            File currentFile = new File(filePath);

            List<ScoredDocument> rankedDocuments = indexer.processQueryBM25(currentFile, 10, bm25_b, bm25_k1);

            String fileName = "";
            try {
                fileName = generateOutputFileName(currentTopic + 1, postingListSize);
                BufferedWriter out = new BufferedWriter(new FileWriter(fileName));

                for (ScoredDocument doc : rankedDocuments) {
                    out.append(generateLineOfOutput(currentTopic + 1, postingListSize, doc));
                }

                out.close();
            } catch (IOException e) {
                log.error("Could not write output file: " + fileName);
            }
        }
    }

    // Filename: <postingListSize>_topic<XX>_group<YY>.txt
    private static String generateOutputFileName(int topicNumber, String postingListSize) {
        StringBuilder sb = new StringBuilder();

        sb.append(postingListSize);
        sb.append("_topic");
        sb.append(Integer.toString(topicNumber));
        sb.append("_groupS.txt");

        return sb.toString();
    }

    // topic<XX> Q0 <doc_id> <rank> <similarity> group<YY>_<postingListSize>
    private static String generateLineOfOutput(int topicNumber, String postingListSize, ScoredDocument doc) {
        StringBuilder out = new StringBuilder();
        out.append("topic");
        out.append(Integer.toString(topicNumber));
        out.append(" Q0 ");
        out.append(doc.getDocument().getDocClass());
        out.append("/");
        out.append(doc.getDocument().getId());
        out.append(" ");
        out.append(Integer.toString(doc.getRank()));
        out.append(" ");
        out.append(Double.toString(doc.getScore()));
        out.append(" groupS_");
        out.append(postingListSize);
        out.append("\n");
        return out.toString();
    }

    private static Arguments processArguments(String[] args) {
        int termFrequencyLowerBound = -1;
        int termFrequencyUpperBound = -1;
        double bm25_b = -1d;
        double bm25_k1 = -1d;
        String postingListSize = "";
        String rootDirectory = "";

        if (args.length != 6) {
            log.debug("Invalid number of arguments. Expected: 6");
            printUsageAndExit();
        }

        try {
            termFrequencyLowerBound = Integer.parseInt(args[0]);
            termFrequencyUpperBound = Integer.parseInt(args[1]);
            postingListSize = args[2];
            bm25_b = Double.parseDouble(args[3]);
            bm25_k1 = Double.parseDouble(args[4]);
            rootDirectory = args[5];
        } catch (Exception e) {
            log.debug("Could not process input arguments. Shutdown program.", e);
            printUsageAndExit();
        }

        if (termFrequencyLowerBound < 0 || termFrequencyUpperBound < 0) {
            log.info("Invalid argument for lower- or upper-bound.");
            printUsageAndExit();
        }

        if (bm25_b < 0d || bm25_b > 1d) {
            log.info("Invalid argument value for BM25_b. The value must be between 0 and 1.");
            printUsageAndExit();
        }

        if (bm25_k1 < 0d) {
            log.info("Invalid argument value for BM25_k1. The value must be >= 0.");
            printUsageAndExit();
        }

        return new Arguments(termFrequencyLowerBound, termFrequencyUpperBound, postingListSize, bm25_b, bm25_k1, rootDirectory);
    }

    private static void printUsageAndExit() {
        log.info("USAGE: ir.Task3 <lowerBound> <upperBound> <postingListSize> <BM25_b> <BM25_k1> <pathToDocumentsRoot>");
        System.exit(-1);
    }

    private static class Arguments {

        private int termFrequencyLowerBound;
        private int termFrequencyUpperBound;
        private String postingListSize;
        private double bm25_b;
        private double bm25_k1;
        private String rootDirectory;

        public Arguments(int termFrequencyLowerBound, int termFrequencyUpperBound, String postingListSize, double bm25_b, double bm25_k1, String rootDirectory) {
            this.termFrequencyLowerBound = termFrequencyLowerBound;
            this.termFrequencyUpperBound = termFrequencyUpperBound;
            this.postingListSize = postingListSize;
            this.bm25_b = bm25_b;
            this.bm25_k1 = bm25_k1;
            this.rootDirectory = rootDirectory;
        }

        public double getBm25_b() {
            return bm25_b;
        }

        public double getBm25_k1() {
            return bm25_k1;
        }

        public String getPostingListSize() {
            return postingListSize;
        }

        public String getRootDirectory() {
            return rootDirectory;
        }

        public int getTermFrequencyLowerBound() {
            return termFrequencyLowerBound;
        }

        public int getTermFrequencyUpperBound() {
            return termFrequencyUpperBound;
        }
    }
}