package ir.helper;

import org.apache.log4j.Logger;

/**
 *
 * @author Michael Sobotka
 */
public class IndexHelper {

    private static final Logger log = Logger.getLogger(IndexHelper.class);

    /**
     * Simple helper function that prints a String[] as DEBUG log.
     *
     * @param strings
     */
    public static void printStringArray(String[] strings) {
        StringBuilder sb = new StringBuilder();
        for (String s : strings) {
            sb.append("|");
            sb.append(s);
        }
        log.debug(sb);
    }
}
