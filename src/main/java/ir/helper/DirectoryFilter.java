package ir.helper;

import java.io.File;
import java.io.FileFilter;

/**
 *
 * @author Michael Sobotka
 */
public class DirectoryFilter implements FileFilter {

    @Override
    public boolean accept(File pathname) {
        if (pathname.isDirectory()) {
            return true;
        } else {
            return false;
        }
    }
}
