package ir.api;

/**
 *
 * @author Michael Sobotka
 */
public interface IWeightingScheme {

    /**
     * Computes the weight of a Term.
     *
     * @param documentCount The number of indexed documents: N
     * @param documentFrequency Indicates the number of documents this Term
     * occurs in: d_f
     * @param termFrequency The number of occurrences of the Term in the
     * Document: t_f
     * @return The computed weight by the concrete WeightingScheme.
     */
    public double computeWeight(int documentCount, int documentFrequency, int termFrequency);
}
