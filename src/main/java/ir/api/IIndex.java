package ir.api;

import ir.indexer.entity.Document;
import ir.indexer.entity.ScoredDocument;
import java.util.List;

/**
 *
 * @author Michael Sobotka
 */
public interface IIndex {

    public static final int DEFAULT_LOWERBOUND = 0;
    public static final int DEFAULT_UPPERBOUND = Integer.MAX_VALUE;

    /**
     * Sets the lower bound for the term frequency. Terms with a lower term
     * frequency are not added to the index.
     *
     * @param lowerBound
     */
    public void setTermFrequencyLowerBound(int lowerBound);

    /**
     * Sets the upper bound for the term frequency. Terms with a higher term
     * frequency are not added to the index.
     *
     * @param upperBound
     */
    public void setTermFrequencyUpperBound(int upperBound);

    /**
     * Adds a Document to the Index.
     *
     * 1. Adds the Terms of the document to the internal dictionary.
     *
     * 2. Keeps record of the documentFrequency of terms.
     *
     * 3. Adds the Document to the posting list of a term.
     *
     * @param document The document to add.
     */
    public void addDocument(Document document);

    /**
     * Computes the weights for each term of the Index.
     *
     * @param weightingScheme An instance of a concrete IWeightingScheme.
     */
    public void computeWeights(IWeightingScheme weightingScheme);

    /**
     * Writes the vector representation of the documents to an output file in
     * ARFF format.
     *
     * @param outputFilePath Path to the desired output file, e.g. "output.arff"
     */
    public void writeArff(String outputFilePath);

    /**
     * Returns the average length of a Document (= average number of terms).
     *
     * @return Average document length.
     */
    public double getAverageDocumentLength();

    /**
     * Precomputes the euclidean length of each document vector and stores them
     * in the Index. It is sufficient to call this function once after all
     * documents were parsed. If the scoring scheme does not require these
     * values, don't call this function at all.
     */
    public void precomputeEuclideanLengths();

    /**
     * Retrieves the n highest ranked Documents for a given query.
     *
     * Uses COSINE similarity.
     *
     * Requires the previous invocation of computeWeights().
     *
     * (Optional): Requires the invocation of preComputeEuclideanLengths().
     *
     * @param query The query as a Document.
     * @param numberOfDocuments The number of documents to retrieve.
     * @return A list of Documents, each with rank and score (ordered by rank).
     */
    public List<ScoredDocument> retrieveTopMatchingDocumentsCosine(Document query, int numberOfDocuments);

    /**
     * Retrieves the n highest ranked Documents for a given query.
     *
     * Uses BM25.
     *
     * Requires NO precomputation steps.
     *
     * @param query The query as a Document.
     * @param numberOfDocuments The number of documents to retrieve.
     * @param bm25_b The value for b in BM25 scoring scheme.
     * @param bm25_k1 The value for k1 in BM25 scoring scheme.
     * @return A list of Documents, each with rank and score (ordered by rank).
     */
    public List<ScoredDocument> retrieveTopMatchingDocumentsBM25(Document query, int numberOfDocuments, double bm25_b, double bm25_k1);
}
