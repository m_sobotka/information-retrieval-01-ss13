package ir.eval;

import ir.helper.DirectoryFilter;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import org.apache.log4j.Logger;

/**
 *
 *
 * @author Michael Sobotka
 */
public class TrecEvaluator {

    private static final Logger log = Logger.getLogger(TrecEvaluator.class);
    private File rootDirectory;

    public TrecEvaluator(File rootDirectory) {
        this.rootDirectory = rootDirectory;
    }

    public void generateEvaluationResults() {
        log.info("Root directory: " + rootDirectory);

        File[] runDirs = rootDirectory.listFiles(new DirectoryFilter());

        for (File f : runDirs) {
            log.debug("Creating evaluation for run: " + f);
            parseEvalDirectory(f);
        }


    }

    private void parseEvalDirectory(File runDir) {
        File evalDir = runDir.listFiles(new EvalDirFilter())[0];
        File[] evalFiles = evalDir.listFiles(new UnavailableTopicsFilter());

        List<List<EvalResult>> allResults = new ArrayList<List<EvalResult>>();
        for (File evalFile : evalFiles) {
            List<EvalResult> evalResult = parseEvalFile(evalFile);
            allResults.add(evalResult);
        }
        
        // generate csv
        generateCSVFile(evalDir, allResults);
    }

    private List<EvalResult> parseEvalFile(File evalFile) {

        FileInputStream fis = null;
        Scanner scanner = null;

        try {
            fis = new FileInputStream(evalFile);
            scanner = new Scanner(fis, "UTF-8");
        } catch (IllegalArgumentException e) {
            log.error("Unsupported encoding.", e);
        } catch (FileNotFoundException e) {
            log.error("File not found.", e);
        }


        List<EvalResult> result = new ArrayList<EvalResult>();
        while (scanner.hasNextLine()) {
            try {
                String col1 = scanner.next();
                String col2 = scanner.next(); // don't know what this is good for
                String col3 = scanner.next();
                result.add(new EvalResult(evalFile.getName(), col1, col3));
            } catch (NoSuchElementException e) {
                break;
            }
        }

        scanner.close();

        return result;
    }

    private void generateCSVFile(File evalDir, List<List<EvalResult>> results) {
        StringBuilder sb = new StringBuilder();
        
        // generate header
        sb.append("measure;");
        for (List<EvalResult> resultPerTopic : results) {
            sb.append(resultPerTopic.get(0).topic);
            sb.append(";");
        }
        sb.deleteCharAt(sb.lastIndexOf(";"));
        sb.append("\n");
        
        // generate data
        for (int i = 0; i < 30; i++) {
            String currentMeasure = results.get(0).get(i).measure;
            
            sb.append(currentMeasure);
            sb.append(";");
            
            for (List<EvalResult> r : results) {
                sb.append(r.get(i).value);
                sb.append(";");
            }
            sb.deleteCharAt(sb.lastIndexOf(";"));
            sb.append("\n");
        }
        
        // write evalOverview.csv
        String csvName = evalDir.getParentFile().getName();
        String filePath = rootDirectory.getAbsolutePath() + File.separator + csvName + ".csv";
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(filePath));
            out.append(sb);
            out.close();
        } catch (IOException e) {
            log.error("Could not write output file: " + filePath);
        }
    }

    private class EvalResult {

        private String topic;
        private String measure;
        private String value;

        public EvalResult(String topic, String measure, String value) {
            this.topic = topic;
            this.measure = measure;
            this.value = value;
        }

        @Override
        public String toString() {
            return "EvalResult{" + "topic=" + topic + ", measure=" + measure + ", value=" + value + '}';
        }
    }

    private class EvalDirFilter implements FilenameFilter {

        @Override
        public boolean accept(File dir, String name) {
            return name.contains("eval");
        }
    }

    private class UnavailableTopicsFilter implements FilenameFilter {

        @Override
        public boolean accept(File dir, String name) {
            if (name.contains("topic8.txt") || name.contains("topic12.txt")) {
                return false;
            }
            return true;
        }
    }
}
