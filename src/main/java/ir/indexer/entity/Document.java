package ir.indexer.entity;

import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author Michael Sobotka
 */
public class Document implements Serializable {

    private String id;
    private String docClass;
    private Map<Term, TermFrequency> terms;
    /**
     * The length of the document is the number of terms w.r.t. their term
     * frequency. Although it can be computed from the "terms" Map, it is
     * precomputed and stored in order to save computing time.
     */
    private int length;

    public Document(String id, String docClass, Map<Term, TermFrequency> terms, int length) {
        this.id = id;
        this.docClass = docClass;
        this.terms = terms;
        this.length = length;
    }

    public String getDocClass() {
        return docClass;
    }

    public void setDocClass(String docClass) {
        this.docClass = docClass;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<Term, TermFrequency> getTerms() {
        return terms;
    }

    public void setTerms(Map<Term, TermFrequency> terms) {
        this.terms = terms;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Document other = (Document) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Document{" + "id=" + id + ", docClass=" + docClass + ", length=" + length + '}';
    }
}
