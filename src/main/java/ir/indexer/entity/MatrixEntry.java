package ir.indexer.entity;

/**
 *
 * @author Michael Sobotka
 */
public class MatrixEntry {

    private int termFrequency;
    private double weight;

    public MatrixEntry(int termFrequency) {
        this.termFrequency = termFrequency;
        this.weight = 0;
    }

    public MatrixEntry(int termFrequency, double weight) {
        this.termFrequency = termFrequency;
        this.weight = weight;
    }

    public int getTermFrequency() {
        return termFrequency;
    }

    public void setTermFrequency(int termFrequency) {
        this.termFrequency = termFrequency;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "[tf=" + termFrequency + ", w=" + weight + ']';
    }

}
