package ir.indexer.entity;

/**
 *
 * @author Michael Sobotka
 */
public class Score implements Comparable<Score> {

    private int documentIndex;
    private double score;

    public Score(int documentIndex, double score) {
        this.documentIndex = documentIndex;
        this.score = score;
    }

    public int getDocumentIndex() {
        return documentIndex;
    }

    public double getScore() {
        return score;
    }

    public void setDocumentIndex(int documentIndex) {
        this.documentIndex = documentIndex;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public int compareTo(Score o) {
        return Double.compare(score, o.score);
    }
}
