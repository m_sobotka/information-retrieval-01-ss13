package ir.indexer.entity;

/**
 *
 * @author Michael Sobotka
 */
public class MatrixTerm {

    private String term;
    private int documentFrequency;
    private int rowIndex;

    public MatrixTerm(String term) {
        this.term = term;
    }

    public MatrixTerm(String term, int documentFrequency, int rowIndex) {
        this.term = term;
        this.documentFrequency = documentFrequency;
        this.rowIndex = rowIndex;
    }

    public void incrementDocumentFrequency() {
        ++documentFrequency;
    }

    public int getDocumentFrequency() {
        return documentFrequency;
    }

    public void setDocumentFrequency(int documentFrequency) {
        this.documentFrequency = documentFrequency;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MatrixTerm other = (MatrixTerm) obj;
        if ((this.term == null) ? (other.term != null) : !this.term.equals(other.term)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + (this.term != null ? this.term.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "MatrixTerm{" + "term=" + term + ", documentFrequency=" + documentFrequency + ", rowIndex=" + rowIndex + '}';
    }
}
