package ir.indexer.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Michael Sobotka
 */
public class Term implements Serializable {

    private String term;
    private int documentFrequency;

    public Term() {
    }

    public Term(String term, int documentFrequency) {
        this.term = term;
        this.documentFrequency = documentFrequency;
    }

    public void incrementDocumentFrequency() {
        ++documentFrequency;
    }

    public int getDocumentFrequency() {
        return documentFrequency;
    }

    public void setDocumentFrequency(int documentFrequency) {
        this.documentFrequency = documentFrequency;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Term other = (Term) obj;
        if ((this.term == null) ? (other.term != null) : !this.term.equals(other.term)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.term != null ? this.term.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append(term);
        sb.append(" | df=");
        sb.append(documentFrequency);
        sb.append("]");
        return sb.toString();
    }
}
