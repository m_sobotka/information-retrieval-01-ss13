package ir.indexer.entity;

import java.io.Serializable;

/**
 * A faster implementation of a MutableInteger. Used to performantly aggregate
 * the termFrequency of a Term.
 *
 * @author Michael Sobotka
 */
public class TermFrequency implements Serializable {

    private int value;

    public TermFrequency() {
        this.value = 1;
    }

    public TermFrequency(int termFrequency) {
        this.value = termFrequency;
    }

    public void increment() {
        ++value;
    }

    public int get() {
        return value;
    }

    @Override
    public String toString() {
        return "[tf=" + value + ']';
    }
}
