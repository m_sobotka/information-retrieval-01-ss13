package ir.indexer.entity;

/**
 *
 * @author Michael Sobotka
 */
public class ScoredDocument {

    private Document document;
    private int rank;
    private double score;

    public ScoredDocument(Document document, int rank, double score) {
        this.document = document;
        this.rank = rank;
        this.score = score;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "ScoredDocument{" + "document=" + document + ", rank=" + rank + ", score=" + score + '}';
    }
}
