package ir.indexer.index;

import cern.colt.list.IntArrayList;
import cern.colt.list.ObjectArrayList;
import cern.colt.matrix.ObjectFactory2D;
import cern.colt.matrix.ObjectMatrix2D;
import ir.api.IIndex;
import ir.api.IWeightingScheme;
import ir.indexer.entity.*;
import ir.indexer.weighting.WeightingTfIdf;
import java.io.File;
import java.io.IOException;
import java.util.*;
import org.apache.log4j.Logger;
import weka.core.*;
import weka.core.converters.ArffSaver;
import weka.core.converters.Saver;

/**
 *
 * @author Michael Sobotka
 */
public class MatrixIndex implements IIndex {

    private static final Logger log = Logger.getLogger(MatrixIndex.class);
    private int termFrequencyLowerBound;
    private int termFrequencyUpperBound;
    private int termCount;
    private int documentCount;
    private int totalLengthOfDocuments;
    private Map<String, MatrixTerm> dictionary;
    private List<MatrixTerm> termLookup;
    private Map<Integer, Document> documents;
    private Double[] euclideanLengths;
    private ObjectMatrix2D sparseMatrix;

    public MatrixIndex() {
        dictionary = new HashMap<String, MatrixTerm>();
        documents = new HashMap<Integer, Document>();
        termLookup = new ArrayList<MatrixTerm>();
        sparseMatrix = ObjectFactory2D.sparse.make(200 * 1000, 8000);
        termCount = 0;
        documentCount = 0;
        totalLengthOfDocuments = 0;
        termFrequencyLowerBound = IIndex.DEFAULT_LOWERBOUND;
        termFrequencyUpperBound = IIndex.DEFAULT_UPPERBOUND;
    }

    @Override
    public synchronized void addDocument(Document document) {
        log.debug("Adding new Document to Index.");

        // the new document will have the current col-index
        int columnIndex = documentCount;

        // add the length of the document to the total length of docs
        totalLengthOfDocuments += document.getLength();

        for (Map.Entry<Term, TermFrequency> t : document.getTerms().entrySet()) {
            Term term = t.getKey();
            TermFrequency termFrequency = t.getValue();

            if (hasInvalidTermFrequency(termFrequency.get())) {
                log.debug("Skipped term because of invalid term frequency: " + termFrequency.get());
                continue;
            }

            if (dictionary.containsKey(term.getTerm())) {
                MatrixTerm mt = dictionary.get(term.getTerm());

                // update the documentFrequency of the Term AND add it to the matrix
                mt.incrementDocumentFrequency();
                sparseMatrix.set(mt.getRowIndex(), columnIndex, new MatrixEntry(termFrequency.get()));

            } else {
                // add the new Term to the dictionary AND to the matrix
                int rowIndex = termCount;
                MatrixTerm newTerm = new MatrixTerm(term.getTerm(), 1, rowIndex);
                dictionary.put(term.getTerm(), newTerm);
                termLookup.add(newTerm);

                sparseMatrix.set(rowIndex, columnIndex, new MatrixEntry(termFrequency.get()));

                termCount++;
            }
        }

        // a new document was added, so increase the counter and add it to the collection
        document.setTerms(null); // help with garbage collection
        documents.put(columnIndex, document);
        documentCount++;

        // Every 100 documents, print some status information
        if (documentCount % 400 == 0) {
            log.info("Documents added: " + documentCount + " | Current dictionary size: " + dictionary.size());
        }
    }

    private boolean hasInvalidTermFrequency(int termFrequency) {
        if (termFrequency < termFrequencyLowerBound || termFrequency > termFrequencyUpperBound) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void computeWeights(IWeightingScheme weightingScheme) {

        for (int row = 0; row < dictionary.size(); row++) {
            IntArrayList colIndices = new IntArrayList();
            ObjectArrayList values = new ObjectArrayList();
            sparseMatrix.viewRow(row).getNonZeros(colIndices, values);

            for (int i = 0; i < colIndices.size(); i++) {
                int colIndex = colIndices.get(i);
                MatrixEntry matrixEntry = (MatrixEntry) sparseMatrix.get(row, colIndex);

                int termFrequency = matrixEntry.getTermFrequency();
                int documentFrequency = termLookup.get(row).getDocumentFrequency();

                double weight = weightingScheme.computeWeight(documentCount, documentFrequency, termFrequency);

                matrixEntry.setWeight(weight);
            }

            if (row % 10000 == 0) {
                log.info("Computed weights for #terms: " + row);
            }
        }
    }

    @Override
    public void precomputeEuclideanLengths() {
        log.info("Precomputing Euclidean lengths. Takes a little...");
        euclideanLengths = new Double[documents.size()];

        int count = 0;

        for (int docIndex = 0; docIndex < documents.size(); docIndex++) {
            computeEuclideanLengthOfDocument(docIndex);
            count++;

            // print progress
            if (count % 1000 == 0) {
                log.info("Computed euclidean lengths: " + count);
            }
        }
    }

    @Override
    public void writeArff(String outputFilePath) {
        int outputDocCount = 0;
        ArffSaver fileWriter;

        try {
            fileWriter = new ArffSaver();
            fileWriter.setFile(new File(outputFilePath));
        } catch (IOException e) {
            log.error("Could not setup ARFF writer.", e);
            return;
        }

        FastVector atts = null;
        double[] vals = null;
        Instances data = null;

        atts = new FastVector();

        // Add @ATTRIBUTEs
        atts.addElement(new Attribute("docClass", (FastVector) null));
        atts.addElement(new Attribute("docId", (FastVector) null));

        for (MatrixTerm t : termLookup) {
            atts.addElement(new Attribute(t.getTerm()));
        }

        data = new Instances("newsgroups", atts, 0);
        fileWriter.setInstances(data);
        fileWriter.setStructure(data);
        fileWriter.setCompressOutput(true);
        fileWriter.setRetrieval(Saver.INCREMENTAL);

        // Add @DATA

        // Iterate over the documents
        for (int col = 0; col < documents.size(); col++) {
            // Fetch docClass & docId
            String docClass = documents.get(col).getDocClass();
            String docId = documents.get(col).getId();

            vals = new double[data.numAttributes()];
            vals[0] = data.attribute(0).addStringValue(docClass);
            vals[1] = data.attribute(1).addStringValue(docId);

            // Fetch weight for each attribute
            for (int row = 0; row < dictionary.size(); row++) {
                MatrixEntry me = (MatrixEntry) sparseMatrix.get(row, col);
                if (me == null) {
                    // vals[row + 2] = Instance.missingValue();
                    // just leave it without concrete value, SparseInstance will handle the rest...
                } else {
                    vals[row + 2] = me.getWeight();
                }
            }

            // Write .arff output file incrementally
            try {
                Instance newInstance = new Instance(1.0, vals);
                newInstance.setDataset(data);
                Instance sparseInstance = new SparseInstance(newInstance);
                sparseInstance.setDataset(data);
                fileWriter.writeIncremental(sparseInstance);

                outputDocCount++;

                // Print some status information
                if (outputDocCount % 200 == 0) {
                    log.info("Documents converted to ARFF: " + outputDocCount + " of " + documents.size());
                }
            } catch (IOException e) {
                log.error("Could not write document to .arff output: " + docId, e);
            } catch (Exception e) {
                log.error("Could not write document to (sparse) .arff output: " + docId, e);
            }
        }
        try {
            fileWriter.writeIncremental(null);
        } catch (IOException e) {
            log.error(e);
        }
    }

    @Override
    public List<ScoredDocument> retrieveTopMatchingDocumentsBM25(Document query, int desiredDocuments, double bm25_b, double bm25_k1) {
        Score[] scores = initializeScoresArray();

        // Iterate over all query terms
        for (Map.Entry<Term, TermFrequency> queryTerm : query.getTerms().entrySet()) {

            MatrixTerm dictionaryTerm = dictionary.get(queryTerm.getKey().getTerm());

            // The term was not found in the dictionary, ignore it
            if (dictionaryTerm == null) {
                continue;
            }

            int rowIndex = dictionaryTerm.getRowIndex();

            int documentFrequency = dictionaryTerm.getDocumentFrequency();
            int queryTermFrequency = queryTerm.getValue().get();

            // fetch the documents where the term is contained
            IntArrayList matchingDocIndices = new IntArrayList();
            ObjectArrayList objects = new ObjectArrayList();
            sparseMatrix.viewRow(rowIndex).getNonZeros(matchingDocIndices, objects);
            log.debug("Matching documents for term: " + queryTerm + " : " + matchingDocIndices.size());

            for (int i = 0; i < matchingDocIndices.size(); i++) {
                int matchingDocIndex = matchingDocIndices.get(i);
                double documentTermFrequency = (double) ((MatrixEntry) sparseMatrix.get(rowIndex, matchingDocIndex)).getTermFrequency();
                double documentLength = (double) getDocumentLengthByDocumentIndex(matchingDocIndex);
                double averageDocumentLength = getAverageDocumentLength();

                double factor1 = Math.log10(documentCount / documentFrequency);
                double factor2 = ((bm25_k1 + 1) * documentTermFrequency) / (bm25_k1 * ((1 - bm25_b) + bm25_b * (documentLength / averageDocumentLength)) + documentTermFrequency);
                // TODO: with parameter k3, we could introduce factor3 (for query length normalization)
                
                double matchingDocumentScore = factor1 * factor2;
                
                scores[matchingDocIndex].setScore(scores[matchingDocIndex].getScore() + matchingDocumentScore);
                log.debug("Matching doc: " + matchingDocIndex + " | weight: " + matchingDocumentScore);
            }
        }

        // Pick the documents with highest score
        List<ScoredDocument> bestRankedDocuments = pickNHighestValues(scores, desiredDocuments);
        return bestRankedDocuments;
    }

    @Override
    public List<ScoredDocument> retrieveTopMatchingDocumentsCosine(Document query, int desiredDocuments) {
        IWeightingScheme weightingScheme = new WeightingTfIdf();
        Score[] scores = initializeScoresArray();

        for (Map.Entry<Term, TermFrequency> queryTerm : query.getTerms().entrySet()) {

            MatrixTerm dictionaryTerm = dictionary.get(queryTerm.getKey().getTerm());

            // The term was not found in the dictionary, ignore it
            if (dictionaryTerm == null) {
                continue;
            }

            int rowIndex = dictionaryTerm.getRowIndex();

            int docCount = documents.size();
            int docFrequency = dictionaryTerm.getDocumentFrequency();
            int queryTermFrequency = queryTerm.getValue().get();

            double query_term_weight = weightingScheme.computeWeight(docCount, docFrequency, queryTermFrequency);

            // fetch the documents where the term is contained
            IntArrayList matchingDocIndices = new IntArrayList();
            ObjectArrayList objects = new ObjectArrayList();
            sparseMatrix.viewRow(rowIndex).getNonZeros(matchingDocIndices, objects);
            log.debug("Matching documents for term: " + queryTerm + " : " + matchingDocIndices.size());

            for (int i = 0; i < matchingDocIndices.size(); i++) {
                int matchingDocIndex = matchingDocIndices.get(i);

                double matchingDocumentWeight = ((MatrixEntry) sparseMatrix.get(rowIndex, matchingDocIndex)).getWeight();
                scores[matchingDocIndex].setScore(scores[matchingDocIndex].getScore() + matchingDocumentWeight * query_term_weight);
            }
        }

        // normalize score by euclidian length (the query is explicitly not normalized)
        for (int i = 0; i < scores.length; i++) {
            if (scores[i].getScore() != 0) {
                scores[i].setScore(scores[i].getScore() / computeEuclideanLengthOfDocument(i));
            }
        }

        // Pick the documents with highest score
        List<ScoredDocument> bestRankedDocuments = pickNHighestValues(scores, desiredDocuments);
        return bestRankedDocuments;
    }

    private List<ScoredDocument> pickNHighestValues(Score[] scores, int desiredElements) {
        log.info("Searching for N highest scores.");
        List<ScoredDocument> result = new ArrayList<ScoredDocument>(desiredElements);

        Arrays.sort(scores);
        for (int i = scores.length - 1; i > scores.length - 1 - desiredElements; i--) {
            int docIndex = scores[i].getDocumentIndex();
            int rank = scores.length - i;
            result.add(new ScoredDocument(documents.get(docIndex), rank, scores[i].getScore()));
        }

        return result;
    }

    private double computeEuclideanLengthOfDocument(int documentIndex) {
        // first look up value in cache
        if (euclideanLengths[documentIndex] != null) {
            return euclideanLengths[documentIndex];
        }
        double resultWeight = 0d;

        IntArrayList rowIndices = new IntArrayList();
        ObjectArrayList objects = new ObjectArrayList();
        sparseMatrix.viewColumn(documentIndex).getNonZeros(rowIndices, objects);

        for (int i = 0; i < rowIndices.size(); i++) {
            int rowIndex = rowIndices.get(i);
            double weight = ((MatrixEntry) sparseMatrix.get(rowIndex, documentIndex)).getWeight();
            resultWeight += weight * weight;
        }

        euclideanLengths[documentIndex] = Math.sqrt(resultWeight);
        return euclideanLengths[documentIndex];
    }

    private Score[] initializeScoresArray() {
        Score[] scores = new Score[documents.size()];
        for (int i = 0; i < scores.length; i++) {
            scores[i] = new Score(i, 0);
        }
        return scores;
    }

    @Override
    public void setTermFrequencyUpperBound(int upperBound) {
        this.termFrequencyUpperBound = upperBound;
    }

    @Override
    public void setTermFrequencyLowerBound(int lowerBound) {
        this.termFrequencyLowerBound = lowerBound;
    }

    @Override
    public double getAverageDocumentLength() {
        return (double) totalLengthOfDocuments / documentCount;
    }

    private int getDocumentLengthByDocumentIndex(int documentIndex) {
        return documents.get(documentIndex).getLength();
    }
}
