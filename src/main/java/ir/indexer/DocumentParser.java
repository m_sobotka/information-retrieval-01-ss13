package ir.indexer;

import ir.api.IIndex;
import ir.indexer.entity.Document;
import ir.indexer.entity.Term;
import ir.indexer.entity.TermFrequency;
import ir.processing.PorterStemmer;
import java.io.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael Sobotka
 */
public class DocumentParser implements Runnable {

    private static final Logger log = Logger.getLogger(DocumentParser.class);
    /**
     * Used to count the TermFrequency for each Term in the document
     */
    private Map<Term, TermFrequency> terms;
    private IIndex index;
    private BlackList blackList;
    private File file;
    private Document doc;
    private int documentLength;

    public DocumentParser(IIndex index, BlackList blackList, File file) {
        this.index = index;
        this.blackList = blackList;
        this.file = file;
        this.terms = new HashMap<Term, TermFrequency>();
        this.documentLength = 0;
    }

    @Override
    public void run() {
        log.debug("Parsing file: " + file);

        // 1. Parse the file.
        parseFile(file);

        // 2. Create the document
        doc = new Document(file.getName(), file.getParentFile().getName(), terms, documentLength);
        log.debug(doc);

        // Once parsing is finished, add the Document to the Index
        if (index != null) {
            index.addDocument(doc);
        }
    }

    private void parseFile(File file) {
        FileInputStream fis = null;
        BufferedReader reader = null;

        try {
            fis = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            log.error("Unsupported encoding.", e);
        } catch (FileNotFoundException e) {
            log.error("File not found.", e);
        }

        // process file line by line
        try {
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }

                // process tokens
                String[] tokens = line.split(" ");
                processTokens(tokens);
            }
        } catch (IOException e) {
            log.error("Could not read file.", e);
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                log.error("Could not close stream.", e);
            }
        }
    }

    private void processTokens(String[] tokens) {
        for (String t : tokens) {

            // 1. Trimming
            t = t.trim();

            // 2. toLowerCase
            t = t.toLowerCase(Locale.ENGLISH);

            // 3. Filtering
            if (blackList.isBlackListed(t)) {
                continue;
            }

            // 4. Remove special chars at the beginning or end of token, e.g. ',the...' becomes 'the'
            t = t.replaceFirst("^[^a-zA-Z0-9]+", "");
            t = t.replaceAll("[^a-zA-Z0-9]+$", "");

            // 4b. Remove unneccessary special chars
            t = t.replaceAll("[^\\x20-\\x7E]", "");

            // 5. Stemming
            PorterStemmer ps = new PorterStemmer();
            ps.add(t.toCharArray(), t.length());
            ps.stem();
            t = ps.toString();

            if (t.isEmpty()) {
                continue;
            }

            // 6. Add term & term frequency
            Term term = new Term(t, 1);
            TermFrequency tf = terms.get(term);
            if (tf == null) {
                terms.put(term, new TermFrequency());
            } else {
                tf.increment();
            }

            // The Term was valid, so increase document length by 1
            documentLength++;
        }
    }

    public Document getDoc() {
        return doc;
    }
}
