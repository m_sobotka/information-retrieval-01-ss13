package ir.indexer;

import ir.api.IIndex;
import ir.api.IWeightingScheme;
import ir.helper.DirectoryFilter;
import ir.indexer.entity.Document;
import ir.indexer.entity.ScoredDocument;
import ir.indexer.index.MatrixIndex;
import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;

/**
 *
 * @author Michael Sobotka
 */
public class Indexer {

    private static final Logger log = Logger.getLogger(Indexer.class);
    private BlackList blackList;
    private IIndex index;
    private ExecutorService executorService;

    public Indexer(int termFrequencyLowerBound, int termFrequencyUpperBound) {
        blackList = new BlackList();
        index = new MatrixIndex();
        index.setTermFrequencyLowerBound(termFrequencyLowerBound);
        index.setTermFrequencyUpperBound(termFrequencyUpperBound);
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
    }

    /**
     * Parses all files contained in "rootDirectory/subdir/document and adds
     * them to the Index.
     *
     * This method is tailored towards the 20_newsgroups_subset from this
     * assignment.
     *
     * @param rootDirectory
     */
    public void parseDirectories(File rootDirectory) {
        File[] directories = rootDirectory.listFiles(new DirectoryFilter());

        for (File dir : directories) {
            log.info("Parsing directory: " + dir);

            for (File file : dir.listFiles()) {
                executorService.execute(new DocumentParser(index, blackList, file));
            }
        }
        waitForParsersToFinish();
    }

    /**
     * Waits until all DoucmentParsers have finished parsing.
     */
    private void waitForParsersToFinish() {
        executorService.shutdown();
        try {
            executorService.awaitTermination(20, TimeUnit.MINUTES);
            log.info("Parsing finished.");
        } catch (InterruptedException e) {
            log.error("Indexing took too long. Shutting down.");
            System.exit(-1);
        }
    }

    /**
     * Computes the weights for each document according to a specific
     * WeightingScheme, e.g. tf-idf.
     */
    public void computeWeights(IWeightingScheme weightingScheme) {
        index.computeWeights(weightingScheme);
    }

    /**
     * Precomputes the euclidean length of each document vector. This might not
     * be required for each scoring scheme.
     */
    public void preComputeEuclideanLengths() {
        index.precomputeEuclideanLengths();
    }

    /**
     * Generates an .arff file with the contents of the Index as described in
     * Task 1.1 of this assignment.
     *
     * @param outputFilePath The path to the output file (will be generated if
     * it not exists).
     */
    public void writeArff(String outputFilePath) {
        index.writeArff(outputFilePath);
    }

    /**
     * Retrieves the n highest ranked Documents for a given query.
     *
     * Uses COSINE similarity.
     *
     * Requires the previous invocation of computeWeights(). (Optional):
     * Requires the invocation of preComputeEuclideanLengths().
     *
     * @param queryFile The file containing the Query input.
     * @param numberOfDocuments The number of documents to retrieve.
     * @return A list of Documents, each with rank and score (ordered by rank).
     */
    public List<ScoredDocument> processQueryCosine(File queryFile, int numberOfDocuments) {
        log.info("Processing Query for input file (COSINE):" + queryFile.getAbsolutePath());
        Document queryDocument = parseQueryDocument(queryFile);
        return index.retrieveTopMatchingDocumentsCosine(queryDocument, numberOfDocuments);
    }

    /**
     * Retrieves the n highest ranked Documents for a given query.
     *
     * Uses BM25.
     *
     * Requires NO precomputation steps.
     *
     * @param queryFile The file containing the Query input.
     * @param numberOfDocuments The number of documents to retrieve.
     * @param bm25_b The value for b in BM25 scoring scheme.
     * @param bm25_k1 The value for k1 in BM25 scoring scheme.
     * @return A list of Documents, each with rank and score (ordered by rank).
     */
    public List<ScoredDocument> processQueryBM25(File queryFile, int numberOfDocuments, double bm25_b, double bm25_k1) {
        log.info("Processing Query for input file (BM25):" + queryFile.getAbsolutePath());
        Document queryDocument = parseQueryDocument(queryFile);
        return index.retrieveTopMatchingDocumentsBM25(queryDocument, numberOfDocuments, bm25_b, bm25_k1);
    }

    /**
     * Returns the average document length.
     *
     * @return Average length of all indexed Documents.
     */
    public double getAverageDocumentLength() {
        return index.getAverageDocumentLength();
    }

    /**
     * Turns a query-file into a Document object.
     *
     * @param queryFile
     * @return
     */
    private Document parseQueryDocument(File queryFile) {
        DocumentParser dp = new DocumentParser(null, blackList, queryFile);
        dp.run();
        Document parsedDocument = dp.getDoc();
        return parsedDocument;
    }
}
