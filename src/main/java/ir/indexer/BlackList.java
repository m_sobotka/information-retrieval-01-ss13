package ir.indexer;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Michael Sobotka
 */
public class BlackList {

    private static Set<String> blackList;

    public BlackList() {
        initializeBlacklist();
    }

    // TODO: This list can be extended.
    private void initializeBlacklist() {
        blackList = new HashSet<String>();
        blackList.add("");
        blackList.add(" ");
        blackList.add(".");
        blackList.add(",");
        blackList.add(">");
        blackList.add("<");
        blackList.add("\"");
    }

    public boolean isBlackListed(String string) {
        if (blackList.contains(string)) {
            return true;
        }
        return false;
    }
}
