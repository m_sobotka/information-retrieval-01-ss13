package ir.indexer.weighting;

import ir.api.IWeightingScheme;

/**
 * Computes: tf-idf_t,d = tf_t,d * idf_t with normalizing tf
 *
 * @author Michael Sobotka
 */
public class WeightingTfIdf implements IWeightingScheme {

    @Override
    public double computeWeight(int documentCount, int documentFrequency, int termFrequency) {
        double idf_t = Math.log10(documentCount / documentFrequency);
        return Math.log10(1 + termFrequency) * idf_t;
    }
}
